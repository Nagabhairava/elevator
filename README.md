Elevator Problem Design
	Assumption - Java 8 Runtime
	ElevatorSolution.java - Main Class/Entry point to read and parse the file. (arguments 1) Location of the file 2) Mode A or B)
	Floor.java - Start Floor, End Floor (i.e., Command)
	Command.java - Starting Floor, List of Commands (i.e., Floor)
	Strategy.java - Abstract Class that solves
	StrategyA.java - Solves the elevator problem in Mode A
	StrategyB.java - Solves the elevator problem in Mode B
	StrategyFactory.java - Creates the strategy based on the Mode.
	StrategyTests.java - JUnit test cases for Mode A and Mode B
	TestData.java - Input data for JUnit tests

elevator.jar - Application jar packaged to run independently
    java -jar elevator.jar <path to input text file> A
    java -jar elevator.jar <path to input text file> B 
