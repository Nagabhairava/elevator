package com.bofa.gfx.elevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class StrategyTests {

	@Test
	public void testModeA() {
		String[] inputs = TestData.inputs[4].split(":");
		Command command = new Command(Integer.parseInt(inputs[0]));
		String[] actions = inputs[1].split(",");
		for (String action : actions) {
			String[] floors = action.split("-");
			command.addFloor(new Floor(Integer.parseInt(floors[0]), Integer.parseInt(floors[1])));
		}
		Strategy strategy = StrategyFactory.getStrategy(Strategy.NAME.A);
		strategy.solve(command);
		assertEquals(strategy.display(), TestData.outputModeA[4]);
	}
	
	@Test
	public void testModeB() {
		String[] inputs = TestData.inputs[4].split(":");
		Command command = new Command(Integer.parseInt(inputs[0]));
		String[] actions = inputs[1].split(",");
		for (String action : actions) {
			String[] floors = action.split("-");
			command.addFloor(new Floor(Integer.parseInt(floors[0]), Integer.parseInt(floors[1])));
		}
		Strategy strategy = StrategyFactory.getStrategy(Strategy.NAME.B);
		strategy.solve(command);
		assertEquals(strategy.display(), TestData.outputModeB[4]);
	}

}
