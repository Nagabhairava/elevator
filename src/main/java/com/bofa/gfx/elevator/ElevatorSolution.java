package com.bofa.gfx.elevator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ElevatorSolution {

	/*
	 * args[0] - path to text file containing multiple set of commands as input
	 * args[1] - Mode A (inefficient), Mode B (efficient) 
	 */
	public static void main(String[] args) {
		List<Command> commands = new ArrayList<>();
		if (args.length!=2) {
			System.out.println("Usage: \n\tjava -jar Elevator.jar <filePath> <mode>");
			System.exit(1);
		}
		
		try (Stream<String> stream = Files.lines(Paths.get(args[0]))) {
			stream.forEach((input) -> {
				String[] inputs = input.split(":");
				Command command = new Command(Integer.parseInt(inputs[0]));
				commands.add(command);
				String[] actions = inputs[1].split(",");
				for (String action : actions) {
					String[] floors = action.split("-");
					command.addFloor(new Floor(Integer.parseInt(floors[0]), Integer.parseInt(floors[1])));
				}
			});
		} catch (IOException e) {
			System.out.println("Unable to open the file conatining commands " + e.getMessage());
		}
		Strategy strategy = StrategyFactory.getStrategy(Strategy.NAME.valueOf(args[1]));
		commands.stream().forEach((cmd) -> {
			strategy.solve(cmd);
			System.out.println(strategy.display());
		});
	}

}