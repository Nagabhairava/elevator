package com.bofa.gfx.elevator;

public class StrategyFactory {
	public static Strategy getStrategy(Strategy.NAME name) {
		Strategy strategy = null;
		switch (name) {
			case A:
				strategy = new StrategyA();
				break;
			case B:
				strategy = new StrategyB();
				break;
		}
		return strategy;
	}
}
