package com.bofa.gfx.elevator;

import java.util.List;

public abstract class Strategy {
	public enum NAME { A, B };
	public enum DIRECTION { DOWN, UNKOWN, UP };
	public List<Integer> visited;
	public abstract void solve(Command command);
	public String display() {
		int count = 0, startingFloor = visited.get(0);
		StringBuilder build = new StringBuilder();
		visited.stream().forEach((i) -> {
			build.append(i.intValue());
			build.append(" ");
		});
		for (int v : visited) {
			count += Math.abs(startingFloor - v);
			startingFloor = v;
		}
		build.append("("+count+")");
		return build.toString();
	}
}
