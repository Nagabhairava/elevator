package com.bofa.gfx.elevator;

import java.util.ArrayList;

public class StrategyA extends Strategy {

	@Override
	public void solve(Command commands) {
		visited = new ArrayList<>();
		visited.add(commands.getStartingFloor());
		commands.getFloors().forEach((floor) -> {
			visited.add(floor.getStart());
			visited.add(floor.getEnd());
		});
	}

}