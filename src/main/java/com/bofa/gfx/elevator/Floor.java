package com.bofa.gfx.elevator;

public class Floor {
	private int start;
	private int end;
	
	public Floor(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public int getEnd() {
		return this.end;
	}
}
