package com.bofa.gfx.elevator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class StrategyB extends Strategy {

	@Override
	public void solve(Command command) {
		visited = new ArrayList<>();
		visited.add(command.getStartingFloor());
		List<DIRECTION> directions = command.getFloors().stream()
									.map((floor) -> floor.getStart() < floor.getEnd() ? 
									DIRECTION.UP : DIRECTION.DOWN)
									.collect(Collectors.toList());
		DIRECTION direction = DIRECTION.UNKOWN;
		SortedSet<Integer> sortedSet = null;
		for (int i=0; i<directions.size(); i++) {
			Floor floor = command.getFloors().get(i);
			if (directions.get(i)!=direction) {
				if (direction!=DIRECTION.UNKOWN) {
					if (visited.get(visited.size()-1).intValue()==sortedSet.first().intValue()) sortedSet.remove(sortedSet.first());
					visited.addAll(sortedSet);
				}
				if (directions.get(i)==DIRECTION.DOWN)
					sortedSet = new TreeSet<>(Comparator.reverseOrder());
				else sortedSet = new TreeSet<>();
				direction = directions.get(i);
			}
			sortedSet.add(floor.getStart());
			sortedSet.add(floor.getEnd());
		}
		if (visited.get(visited.size()-1).intValue()==sortedSet.first().intValue()) sortedSet.remove(sortedSet.first());
		if (direction!=DIRECTION.UNKOWN) visited.addAll(sortedSet);
	}

}
