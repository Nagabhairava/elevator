package com.bofa.gfx.elevator;

import java.util.ArrayList;
import java.util.List;

public class Command {
	private int startingFloor;
	private List<Floor> floors;
	
	public Command(int startingFloor) {
		this.startingFloor = startingFloor;
		this.floors = new ArrayList<>();
	}
	
	public int getStartingFloor() {
		return this.startingFloor;
	}
	
	public void addFloor(Floor floor) {
		this.floors.add(floor);
	}
	
	public List<Floor> getFloors() {
		return this.floors;
	}
}